<?php

require_once 'vendor/autoload.php';

const ELEMENT_MINIMUM_LENGTH = 40;
const ELEMENTS_AMOUNT_FOR_ONE_TYPE = 25; // 40 * 25 = 2000
const RACERS_NUMBER = 5;
const MINIMAL_SPEED = 4;
const TOTAL_SPEED = 22;

/**
 * Prepare track elements
 */
$straight = new Game\Entity\Element\Straight();
$curve = new Game\Entity\Element\Curve();

$elements = [$straight, $curve];
$totalElements = count($elements);

/**
 * Generate random track
 */
$track = \Game\Collection\Track::makeForElements($straight, $curve);
$track->generate(ELEMENT_MINIMUM_LENGTH, ELEMENTS_AMOUNT_FOR_ONE_TYPE);

/**
 * Prepare validators to detect cheetters
 */
$speedValidators = [];
$speedValidators[] = new \Game\Validation\MinSpeedSpeedValidator(MINIMAL_SPEED);
$speedValidators[] = new \Game\Validation\TotalSpeedSpeedValidator(TOTAL_SPEED);

/**
 * Generate racers
 */
$cars = [];
foreach (range(1, RACERS_NUMBER) as $carNumber) {
    $speeds = \Game\Generator\SpeedGenerator::generate(MINIMAL_SPEED, TOTAL_SPEED, $totalElements);

    $speed = new \Game\Settings\Speed();
    foreach (range(0, $totalElements - 1) as $index) {
        $speed->setValueOf($elements[$index], $speeds[$index]);
    }

    $speed->setSpeedRules(...$speedValidators);

    $cars[] = \Game\Entity\Car::make($speed);
}
$racers = new \Game\Collection\Racers(...$cars);

/**
 * Enjoy the race
 */
$race = new \Game\Race($track, $racers);
$raceResults = $race->race();
$roundResults = $raceResults->getRoundResults();

/**
 * Results
 */
$lastRoundResults = end($roundResults);
echo 'RESULTS:' , PHP_EOL;
foreach ($lastRoundResults->carsPosition as $car => $position) {
    echo 'CAR #' , ($car + 1), ': ', $position, PHP_EOL;
}
$scores = array_keys($lastRoundResults->carsPosition, max($lastRoundResults->carsPosition));
echo PHP_EOL, count($scores) > 1 ? "IT'S A DRAW" : "THE WINNER: CAR #" . ($scores[0] + 1), PHP_EOL;
