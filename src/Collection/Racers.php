<?php

namespace Game\Collection;

use Game\Entity\Car;

/**
 * Class Racers
 * @package Game
 */
final class Racers implements RacersInterface
{
    /**
     * Track elements configuration
     *
     * @var array|Car[]
     */
    private array $cars = [];

    public function __construct(Car ...$cars)
    {
        $this->cars = $cars;
    }

    /**
     * Get racers
     *
     * @return array
     */
    public function getRacers(): array
    {
        return $this->cars;
    }
}