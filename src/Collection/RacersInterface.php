<?php

namespace Game\Collection;

/**
 * Interface RacersInterface
 * @package Game\Collection
 */
interface RacersInterface
{
    /**
     * @return array
     */
    public function getRacers(): array;
}