<?php

namespace Game\Collection;

use Game\Entity\Element\Element;

/**
 * Class Track
 * @package Game
 */
final class Track implements TrackInterface
{
    /**
     * Track elements configuration
     *
     * @var array|Element[]
     */
    private array $settings = [];

    /**
     * Track elements
     *
     * @var array
     */
    private array $elements = [];

    /**
     * Initialize Track with elements settings
     *
     * @param Element ...$settings
     */
    private function __construct(Element ...$settings)
    {
        $this->settings = $settings;
    }

    /**
     * Create track elements according to the configuration
     *
     * @param Element ...$settings
     * @return Track
     */
    public static function makeForElements(Element ...$settings): Track
    {
        return new Track(...$settings);
    }

    /**
     * Generate track
     *
     * @param int $minimumLength
     * @param int $amount
     */
    public function generate(int $minimumLength = 40, int $amount = 25): void
    {
        /**
         * Generate series of elements according to the settings
         */
        $series = [];
        foreach ($this->settings as $element) {
            $series = array_merge($series, array_fill(0, $amount, $element));
        }

        /**
         * Place series in random order
         */
        shuffle($series);

        /**
         * Link elements with each other
         */
        $position = 0;
        $previousElement = null;
        foreach ($series as $templateElement) {
            for ($step = 0; $step < $minimumLength; $step++, $position++) {
                $element = clone $templateElement;
                $element->setPosition($position);

                if ($previousElement !== null) {
                    $previousElement->setNext($element);
                    $element->setPrev($previousElement);
                }

                $previousElement = $element;

                $this->elements[] = $element;
            }
        }
    }

    /**
     * Get track elements
     *
     * @return array
     */
    public function getElements(): array
    {
        return $this->elements;
    }
}