<?php

namespace Game\Collection;

/**
 * Interface TrackInterface
 * @package Game\Collection
 */
interface TrackInterface
{
    public function generate(int $minimumLength = 40, int $amount = 25): void;
}