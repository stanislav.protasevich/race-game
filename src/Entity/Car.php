<?php

namespace Game\Entity;

use Game\Settings\Speed;

/**
 * Class Car
 * @package Game\Entity
 */
class Car
{
    /**
     * Car's speed
     * @var Speed
     */
    protected Speed $speed;

    /**
     * @param Speed $speed
     */
    protected function __construct(Speed $speed)
    {
        $this->setSpeed($speed);
    }

    /**
     * @param Speed $speed
     * @return static
     */
    public static function make(Speed $speed): self
    {
        return new static($speed);
    }

    /**
     * @param Speed $speed
     * @return $this
     */
    public function setSpeed(Speed $speed): self
    {
        $this->speed = $speed;

        return $this;
    }

    /**
     * @return Speed
     */
    public function getSpeed(): Speed
    {
        return $this->speed;
    }
}
