<?php

namespace Game\Entity\Element;

/**
 * Class Base
 * @package Game\Entity\Element
 */
abstract class Base implements Element
{
    /**
     * Element position
     *
     * @var int|null
     */
    protected ?int $position;

    /**
     * Previous element
     *
     * @var Element|null
     */
    protected ?Element $prev;

    /**
     * Next element
     *
     * @var Element|null
     */
    protected ?Element $next;

    /**
     * @return ?Element
     */
    public function getPrev(): ?Element
    {
        return $this->prev ?? null;
    }

    /**
     * @param Element $prev
     * @return Element
     */
    public function setPrev(Element $prev): Element
    {
        $this->prev = $prev;

        return $this;
    }

    /**
     * @return ?Element
     */
    public function getNext(): ?Element
    {
        return $this->next ?? null;
    }

    /**
     * @param Element $next
     *
     * @return Element
     */
    public function setNext(Element $next): Element
    {
        $this->next = $next;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getPosition(): ?int
    {
        return $this->position;
    }

    /**
     * @param int|null $position
     */
    public function setPosition(?int $position): void
    {
        $this->position = $position;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return static::class;
    }
}