<?php

namespace Game\Entity\Element;

interface Element
{
    public function getPrev(): ?Element;

    public function setPrev(Element $prev): Element;

    public function getNext(): ?Element;

    public function setNext(Element $next): Element;

    public function getPosition(): ?int;

    public function setPosition(?int $position): void;

    public function getType(): string;
}
