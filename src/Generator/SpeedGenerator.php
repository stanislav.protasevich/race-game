<?php

namespace Game\Generator;

/**
 * Class SpeedGenerator
 * @package Game\Generator
 */
class SpeedGenerator implements SpeedGeneratorInterface
{
    /**
     * Generate speed configuration according to technical requirements
     *
     * @param int $minimalSpeed
     * @param int $totalSpeed
     * @param int $limit
     *
     * @return array
     */
    public static function generate(int $minimalSpeed, int $totalSpeed, int $limit): array
    {
        $data = array_fill(0, $limit, null);

        for ($iteration = 0; $iteration < $limit; $iteration++) {
            if ($iteration === $limit - 1) {
                $data[$iteration] = $totalSpeed;
                break;
            }

            $from = $minimalSpeed;

            if(($to = $totalSpeed - (($limit - $iteration) * $minimalSpeed)) <= 0) {
                $to = 1;
            }

            $data[$iteration] = rand($from, $to);

            $totalSpeed -= $data[$iteration];
        }

        return $data;
    }
}
