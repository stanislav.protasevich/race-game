<?php

namespace Game\Generator;

/**
 * Interface SpeedGeneratorInterface
 * @package Game\Generator
 */
interface SpeedGeneratorInterface
{
    /**
     * @param int $minimalSpeed
     * @param int $totalSpeed
     * @param int $limit
     *
     * @return array
     */
    public static function generate(int $minimalSpeed, int $totalSpeed, int $limit): array;
}
