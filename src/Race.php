<?php

namespace Game;

use Game\Entity\Car;
use Game\Collection\TrackInterface;
use Game\Collection\RacersInterface;

/**
 * Class Race
 * @package Game
 */
class Race
{
    /**
     * Race track
     *
     * @var TrackInterface
     */
    protected TrackInterface $track;

    /**
     * Race participants
     *
     * @var RacersInterface
     */
    protected RacersInterface $racers;

    /**
     * Game initialization
     *
     * @param TrackInterface $track
     * @param RacersInterface $racers
     */
    public function __construct(TrackInterface $track, RacersInterface $racers)
    {
        $this->track = $track;
        $this->racers = $racers;

        /** @var Car $racer */
        foreach ($racers->getRacers() as $racer) {
            $racer->getSpeed()->validate();
        }
    }

    /**
     * @return RaceResult
     */
    public function race(): RaceResult
    {
        $raceResults = new RaceResult();

        /**
         * Prepare game elements
         */
        $racers = $this->racers->getRacers();
        $elements = $this->track->getElements();
        $trackLength = count($elements);

        /**
         * 3.. 2.. 1..
         */
        $coordinates = [];
        foreach ($racers as $racerCar) {
            $coordinates[] = $elements[0];
        }
        $raceResults->addRoundResult(new RoundResult(0, array_fill(0, count($racers), 0)));

        /**
         * Go go go!
         */
        $round = 1;
        $isFinished = false;
        do {
            $carsPosition = [];
            foreach ($racers as $racerNumber => $racerCar) {
                $element = $coordinates[$racerNumber];

                /**
                 * Move car according to speed
                 */
                $steps = $racerCar->getSpeed()->getValueOf($element);
                while ($steps-- > 0) {
                    /**
                     * Stop if the track element hasn't next element
                     */
                    if (null === ($nextElement = $element->getNext())) {
                        $isFinished = true;
                        break;
                    }

                    /**
                     * Stop car moving if we are on element of different type
                     */
                    if ($element->getType() !== $nextElement->getType()) {
                        $element = $nextElement;
                        break;
                    }

                    $element = $nextElement;

                    /**
                     * Stop game if we are on finish
                     */
                    if ($element->getPosition() == $trackLength - 1) {
                        $isFinished = true;
                        break;
                    }
                }

                $carsPosition[] = $element->getPosition();

                $coordinates[$racerNumber] = $element;
            }

            $raceResults->addRoundResult(new RoundResult($round, $carsPosition));

            $round++;
        } while ($isFinished === false);

        return $raceResults;
    }
}
