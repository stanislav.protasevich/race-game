<?php

namespace Game;

class RaceResult
{
    /**
     * @var array of StepResult
     */
    private $roundResults = [];

    public function getRoundResults(): array
    {
        return $this->roundResults;
    }

    public function addRoundResult(RoundResult $roundResult)
    {
        $this->roundResults[] = $roundResult;
    }
}
