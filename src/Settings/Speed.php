<?php

namespace Game\Settings;

use InvalidArgumentException;
use Game\Entity\Element\Element;
use Game\Validation\SpeedValidatorInterface;

/**
 * Class Speed
 * @package Game\Settings
 */
class Speed implements SpeedInterface
{
    /**
     * Speed configuration
     *
     * @var array
     */
    protected array $settings = [];

    /**
     * Speed validators
     *
     * @var array
     */
    protected array $validators = [];

    /**
     * Get speed for element
     *
     * @param Element $element
     * @return int
     */
    public function getValueOf(Element $element): int
    {
        return $this->settings[$element->getType()] ?? 0;
    }

    /**
     * Set speed for element
     *
     * @param Element $element
     * @param int $value
     * @return $this
     */
    public function setValueOf(Element $element, int $value): self
    {
        $this->settings[$element->getType()] = $value;

        return $this;
    }

    /**
     * Get speed settings
     *
     * @return array
     */
    public function getSessings()
    {
        return $this->settings;
    }

    /**
     * Set speed rules
     *
     * @param SpeedValidatorInterface ...$validators
     */
    public function setSpeedRules(SpeedValidatorInterface ...$validators)
    {
        $this->validators = $validators;
    }

    /**
     * Check speed rules
     *
     * @throws InvalidArgumentException
     */
    public function validate()
    {
        /** @var SpeedValidatorInterface $validator */
        foreach ($this->validators as $validator) {
            if (!$validator->isValid($this)) {
                throw new InvalidArgumentException($validator->getMessages());
            }
        }
    }
}
