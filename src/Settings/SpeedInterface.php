<?php

namespace Game\Settings;

use InvalidArgumentException;
use Game\Entity\Element\Element;

/**
 * Interface SpeedInterface
 * @package Game\Settings
 */
interface SpeedInterface
{
    /**
     * @param Element $element
     * @return int
     */
    public function getValueOf(Element $element): int;

    /**
     * @param Element $element
     * @param int $value
     * @return $this
     */
    public function setValueOf(Element $element, int $value): self;

    /**
     * @throws InvalidArgumentException
     */
    public function validate();
}