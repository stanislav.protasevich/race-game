<?php

namespace Game\Validation;

use Game\Settings\Speed;

/**
 * Class MinValidator
 * @package Game\Validation
 */
class MinSpeedSpeedValidator implements SpeedValidatorInterface
{
    /**
     * Minimum value
     *
     * @var mixed
     */
    protected $minSpeed;

    /**
     * Sets validator options
     */
    public function __construct(int $minSpeed)
    {
        $this->minSpeed = $minSpeed;
    }

    public function isValid(Speed $speed)
    {
        $isValid = true;
        foreach ($speed->getSessings() as $setting) {
            if ($setting < $this->minSpeed) {
                $isValid = false;
                break;
            }
        }

        return $isValid;
    }

    public function getMessages()
    {
        return sprintf('The speed need to be greater than or equal to %s', $this->minSpeed);
    }
}
