<?php

namespace Game\Validation;

use Game\Settings\Speed;

/**
 * Interface SpeedValidatorInterface
 * @package Game\Validation
 */
interface SpeedValidatorInterface
{
    /**
     * @param Speed $speed
     * @return mixed
     */
    public function isValid(Speed $speed);

    /**
     * @return mixed
     */
    public function getMessages();
}
