<?php

namespace Game\Validation;

use Game\Settings\Speed;

/**
 * Class TotalSpeedValidator
 * @package Game\Validation
 */
class TotalSpeedSpeedValidator implements SpeedValidatorInterface
{
    /**
     * Total value
     *
     * @var int
     */
    protected int $totalSpeed;

    /**
     * Sets validator options
     *
     * @param int $totalSpeed
     */
    public function __construct(int $totalSpeed)
    {
        $this->totalSpeed = $totalSpeed;
    }

    /**
     * @param Speed $speed
     * @return bool
     */
    public function isValid(Speed $speed): int
    {
        return array_sum($speed->getSessings()) === $this->totalSpeed;
    }

    /**
     * @return string
     */
    public function getMessages(): string
    {
        return sprintf('The speed need to be greater than or equal to %s', $this->totalSpeed);
    }
}
